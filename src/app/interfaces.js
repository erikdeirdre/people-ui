export interface IPerson {
    id: String;
    firstName: String;
    lastName: String;
    address1: String;
    address2: String;
    city: String;
    state: String;
    zipCode: String;
    telephone: String;
    email: String;
    gender: String;
    birthDate: Date;
    age: Number;
}
