import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, AbstractControlOptions,
         FormControl, Validators } from '@angular/forms';
import { checkAgeGroup, checkYearBorn } from '../shared/custom.validator';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  registration: boolean = true;
  currentYear: number = new Date().getFullYear();

  Positions: any = ['Goalkeeper', 'Right Fullback', 'Left Fullback', 
  'Center Back', 'Sweeper', 'Defending/Hoding Midfielder', 
  'Right Midfielder/Winger', 'Central Midfilder', 'Striker',
  'Attacking Midfielder', 'Left Midfielder/Winder'];
  registerForm: FormGroup;

  constructor(private fb: FormBuilder) {
    this.registerForm = this.fb.group({
      email: ['', { validator: [Validators.email, Validators.required] } as AbstractControlOptions],
      firstName: [''],
      lastName: [''],
      phone: [''],
      street: [''],
      city: [''],
      state: [''],
      postalCode: [''],
      player: this.fb.group({
        yearBorn: [''],
        playerInfo: this.fb.array([]),
      }, { validator: [checkAgeGroup, checkYearBorn] } as AbstractControlOptions),
      coach: this.fb.array([]),
      referee: this.fb.array([]),
      }
    );
   }

  ngOnInit(): void {
    this.addBlankPlayer();
    this.addBlankCoach();
    this.addBlankReferee();
  }

  get player() : FormGroup {
    return this.registerForm.get('player') as FormGroup;
  }

  get playerInfo() : FormArray {
    return this.registerForm.get("player.playerInfo") as FormArray;
  }

  get coach() : FormArray {
    return this.registerForm.get("coach") as FormArray;
  }

  get referee() : FormArray {
    return this.registerForm.get("referee") as FormArray;
  }

  changeAgeGroup() {
    const yearBorn = this.registerForm.get['yearBorn'];
    if (yearBorn < 1900) {
      return;
    }

    this.playerInfo.controls.forEach(element => {
      if (element.value['ageGroup'] === '' && yearBorn != '') {
        const age = new Date().getFullYear() - yearBorn;
        element.patchValue({
          ageGroup: age
        })
      }
    });
  }

  onSubmit() {
    console.warn(this.registerForm.value);
  }

  addBlankPlayer() {

    this.playerInfo.push(this.fb.group({
      teamName: new FormControl (""),
      position: new FormControl (""),
      ageGroup: new FormControl (""),
      yearJoined: new FormControl ("")
    }));
  }

  addBlankCoach() {
    this.coach.push(this.fb.group({
      teamName: new FormControl (""),
      ageGroup: new FormControl (""),
      yearJoined: new FormControl ("")      
    }));
  }

  addBlankReferee() {
    this.referee.push(this.fb.group({
      certLevel: new FormControl (""),
      certId: new FormControl (""),
      yearAcquired: new FormControl (""),
      expLevel: new FormControl ("")
    }));
  }

  deleteTeamPlayer(playerIndex: number) {
    this.playerInfo.removeAt(playerIndex);
  }

  deleteTeamCoach(coachIndex: number) {
    this.coach.removeAt(coachIndex);
  }

  deleteRefereeCert(refereeIndex: number) {
    this.referee.removeAt(refereeIndex);
  }

  editTeamPlayer(playerIndex: number) {
    console.log('edit player: ' + playerIndex);
  }

  editTeamCoach(coachIndex: number) {
    console.log('edit coach: ' + coachIndex);
  }

  editRefereeCert(refereeIndex: number) {
    console.log('edit referee: ' + refereeIndex);
  }
}