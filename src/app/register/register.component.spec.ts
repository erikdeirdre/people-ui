import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';

import { RegisterComponent } from './register.component';

describe('RegisterComponent', () => {
  let component: RegisterComponent;
  let fixture: ComponentFixture<RegisterComponent>;
  let fb: FormBuilder;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegisterComponent ],
      providers: [
//        { provide: MatDialog, useValue: {} },
        FormBuilder
//        { provide: Apollo, useValue: spy }
      ]
    })
    .compileComponents();

    fb = TestBed.inject(FormBuilder);

  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('registration form exists', () => {
    expect(component.registerForm).toBeDefined();
    expect(component.registerForm.get("player")).toBeDefined();
    expect(component.registerForm.get("coach")).toBeDefined();
    expect(component.registerForm.get("referee")).toBeDefined();
    expect(component.registerForm.get("email")).toBeDefined();
    expect(component.registerForm.get("firstName")).toBeDefined();
    expect(component.registerForm.get("lastName")).toBeDefined();
    expect(component.registerForm.get("phone")).toBeDefined();
    expect(component.registerForm.get("street")).toBeDefined();
    expect(component.registerForm.get("city")).toBeDefined();
    expect(component.registerForm.get("postalCode")).toBeDefined();
    expect(component.registerForm.get("state")).toBeDefined();
  });

  it('set player registration form', () => {
    const testYear: string = '1980';
    component.registerForm.get('player.yearBorn').setValue(testYear);    
    expect(component.registerForm.get('player.yearBorn').value).toEqual(testYear);
  });

  it('INVALID Year Born - Alpha Character', () => {
    const testYear: string = '199X';
    component.registerForm.get('player.yearBorn').setValue(testYear);
    expect(component.registerForm.get('player.yearBorn').value).toEqual(testYear);
    const yearCheck = component.registerForm.get("player");
    expect(yearCheck.valid).toBeFalsy();
    expect(yearCheck.getError('yearBornCheck')).toBeTruthy();
  });

  it('INVALID Year Born - Future', () => {
    const testYear: string = (new Date().getFullYear() + 1).toString();
    component.registerForm.get('player.yearBorn').setValue(testYear);
    expect(component.registerForm.get('player.yearBorn').value).toEqual(testYear);
    const yearCheck = component.registerForm.get("player");
    expect(yearCheck.valid).toBeFalsy();
    expect(yearCheck.getError('yearBornCheck')).toBeTruthy();
  });

  it('INVALID Year Born - Within two years', () => {
    const testYear: string = (new Date().getFullYear() - 2).toString();
    component.registerForm.get('player.yearBorn').setValue(testYear);
    expect(component.registerForm.get('player.yearBorn').value).toEqual(testYear);
    const yearCheck = component.registerForm.get("player");
    expect(yearCheck.valid).toBeFalsy();
    expect(yearCheck.getError('yearBornCheck')).toBeTruthy();
  });

  it('VALID Year Born', () => {
    const testYear: string = '2000';
    component.registerForm.get('player.yearBorn').setValue(testYear);
    expect(component.registerForm.get('player.yearBorn').value).toEqual(testYear);
    const yearCheck = component.registerForm.get("player");
    expect(yearCheck.valid).toBeFalsy();
    expect(yearCheck.getError('yearBornCheck')).toBeFalsy();
  });

//  it('VALID Age Group', () => {
//    const testYear: string = '2010';
//    const testGroup: string = '10';
//    component.registerForm.get('player.yearBorn').setValue(testYear);
//    for (let info of component.registerForm.get('player.playerInfo').value) {
//      info['ageGroup'] = testGroup;
//    }    expect(component.registerForm.get('player.ageGroup').value).toEqual(testGroup);
//    const ageCheck = component.registerForm.get("player");
//    expect(ageCheck.valid).toBeFalsy();
//    expect(ageCheck.getError('ageCheck')).toBeFalsy();
//  });
//
//  it('INVALID Age Group', () => {
//    const testYear: string = '2010';
//    const testGroup: string = '12';
//    component.registerForm.get('player.yearBorn').setValue(testYear);
//    for (let info of component.registerForm.get('player.playerInfo').value) {
//      info['ageGroup'] = testGroup;
//    }
//    expect(component.registerForm.get('player.ageGroup').value).toEqual(testGroup);
//    const ageCheck = component.registerForm.get("player");
//    expect(ageCheck.valid).toBeFalsy();
//    expect(ageCheck.getError('ageCheck')).toBeFalsy();
//  });
});
