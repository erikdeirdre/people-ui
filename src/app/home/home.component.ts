import { Component, OnInit } from '@angular/core';
import { faTrash, faCoffee, faHome } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  faHome = faHome;
  faCoffee =faCoffee;

  constructor() { }

  ngOnInit(): void {
  }

}
