import { gql } from 'apollo-angular';

export const GET_CITYSTATE = gql`
query GetCityState($postalCode: String!) {
  cityState(postalcode: $postalCode) {
    city
    state
  }
}
`;