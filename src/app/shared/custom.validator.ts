import { AbstractControl, ValidationErrors } from "@angular/forms";

// Functions return 'false' instead of null in order to avoid TypeError: Cannot read property
// messages on validations when the error doesn't exist.
export function checkAgeGroup(control: AbstractControl): ValidationErrors | null {
  const currentYear = new Date().getFullYear();
  const yearBorn = control.get("yearBorn").value;
  const playerInfo = control.get("playerInfo").value;

  if (yearBorn > 1900) {
    for (let info of playerInfo) {
      if (info.ageGroup > 0) {
        let diff = currentYear - info.ageGroup;
        if (diff > yearBorn) {
          return {'ageCheck': true}
        }
      } 
    }
  }

  return { 'ageCheck': false }
}

export function checkYearBorn(control: AbstractControl): ValidationErrors | null {
  const currentYear = new Date().getFullYear() - 2;
  const yearBorn = control.get("yearBorn").value;
  if (isNaN(yearBorn)) {
    return { 'yearBornCheck': true }
  }

  if (yearBorn > 1900) {
    if (yearBorn >= currentYear) {
      return { 'yearBornCheck': true }
    }
  }
  return { 'yearBornCheck': false }
}
