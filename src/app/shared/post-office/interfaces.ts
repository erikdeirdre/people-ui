export interface IAddress {

}

export interface CityState {
  zipCode: string;
  countryCode: string;
  latitude: string;
  longitude: string;
  city: string;
  state: string;
  stateCode: string;
  province: string;
  provinceCode: string;
}

export interface CityStateQueryResponse {
  cityState: CityState;
}

export interface ValidateAddressResponse {
  cityState: CityState;
}