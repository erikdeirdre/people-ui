import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

import { catchError, switchMap, tap } from "rxjs/operators";
//import * as xml2js from 'xml2js';

import { ValidateAddressResponse, CityStateQueryResponse } from './interfaces';
import { Query, gql } from 'apollo-angular';
import { GET_CITYSTATE } from '../../shared/graphql-queries/post-office';

@Injectable({
  providedIn: 'root'
})

export class GetCityState extends Query<CityStateQueryResponse> {
  document = gql`
    query GetCityState($postalCode: String!) {
      cityState(postalcode: $postalCode) {
        city
        state
      }
    }
    `;
}

export class ValidateAddress extends Query<ValidateAddressResponse> {
  document = gql`
    query ValidateAddress($postalCode: String!) {
      cityState(postalcode: $postalCode) {
        city
        state
      }
    }
  `;
}  
  //validAddress(street: string, zipCode: string) {
  //  const endPoint = this.URL + 'API=Verify&XML=<AddressValidateRequest USERID="' + this.USERID 
  //    + '"> <Revision>1</Revision><Address ID="0"><Address1>' + street + '</Address1><Address2></Address2>' 
  //    + '<City/><State></State><Zip5>' + zipCode + '</Zip5><Zip4/></Address></AddressValidateRequest>';
    
  //  return this.httpClient
  //  .get(endPoint, { responseType: "text" })
  //  .pipe(
  //    switchMap(async xml => await this.parseXmlToJson(xml))
  //  );
  //  return street;
  //}
