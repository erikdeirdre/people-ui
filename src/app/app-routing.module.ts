import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProfileComponent } from './profile/profile.component';
import { RegisterComponent } from './register/register.component';
import { HomeComponent } from './home/home.component';
import { TestPostOfficeComponent } from './test-post-office/test-post-office.component';

const routes: Routes = [
  {path: "home", component: HomeComponent},
  {path: "profile", component: ProfileComponent},
  {path: "register", component: RegisterComponent},
  {path: "postoffice", component: TestPostOfficeComponent},
  {path:  "", pathMatch: "full", redirectTo: "home"}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }