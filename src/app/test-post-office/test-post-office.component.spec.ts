import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { ApolloTestingModule, ApolloTestingController} from 'apollo-angular/testing';
import { gql } from 'apollo-angular';
import { TestPostOfficeComponent } from './test-post-office.component';

const GET_CITYSTATE = gql`
  query GetCityState($postalCode: String!) {
    cityState(postalcode: $postalCode) {
    city
    state
  }
}
`;

describe('TestPostOfficeComponent', () => {
  let component: TestPostOfficeComponent;
  let fixture: ComponentFixture<TestPostOfficeComponent>;
  let controller: ApolloTestingController;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ 
        FormsModule,
        ApolloTestingModule
      ],
      declarations: [ TestPostOfficeComponent ]
    })
    .compileComponents();

    controller = TestBed.inject(ApolloTestingController);

  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TestPostOfficeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

//  afterEach(() => {
//    controller.verify();
//  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

//  it('expect and answer', () => {
//    //Call the relevant method
//    component.testZipCode('12345').subscribe((address) => {
//      expect(address.city).toEqual('Mickey');
//      expect(address.state).toEqual('Mouse');
//    });
//
//    const op = controller.expectOne(GET_CITYSTATE);
//
//    // Respond with mock data, causing Observable to resolve.
//    op.flush({
//      data: {
//        citystate: {
//          city: 'Mickey',
//          state: 'Mouse',
//        },
//      },
//    });
//  
//    // Finally, assert that there are no outstanding operations.
//    controller.verify();
//  });
});
