import { Component, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { Apollo, QueryRef, gql } from 'apollo-angular';

import { GetCityState } from '../shared/post-office/post-office.service';

import { IAddress, CityState, CityStateQueryResponse } from '../shared/post-office/interfaces';

const GET_CITYSTATE = gql`
  query GetCityState($postalCode: String!) {
    cityState(postalcode: $postalCode) {
    city
    state
  }
}
`;

@Component({
  selector: 'app-test-post-office',
  templateUrl: './test-post-office.component.html',
  styleUrls: ['./test-post-office.component.css']
})

export class TestPostOfficeComponent implements OnInit {
  loading: boolean;
  zipCode = '';
  street = '';
  city = 'Hanover';
  state = 'NH';
  address: IAddress;
//  cityState: Observable<any>;
  cityState: Observable<CityState>;
  addressQuery: QueryRef<any>;

  private querySubscription: Subscription;

//  constructor(private postOffice: PostOfficeService) { }
  constructor(private getCityState: GetCityState) {}

  ngOnInit() {
    this.cityState = this.getCityState.watch()
    .valueChanges
    .pipe (
      map(result => result.data.cityState)
    )
  }

  testZipCode(zipCode: string) {
    this.addressQuery.refetch()
  }

  ngOnDestroy() {
    this.querySubscription.unsubscribe();
  }
}
