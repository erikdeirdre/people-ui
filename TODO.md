# TO DO LIST

## NavBar

- [X] Display Profile or Register. Not both at the same time.

## Profile Component

### General

- [ ] Clone Register Template

## Register Component

### General

- [ ] Make asterisks red and bold
- [ ] Populate city and state when valid postal code entered
- [ ] Display error when invalid postal code entered
- [X] Remove 'TO DO' list

### Player Details

- [X] Mark Fields required
- [X] Create another line when 'Add Team' is clicked
- [X] Remove line when 'Delete' is clicked
- [X] Remove 'Pencil' icon
- [ ] Send data to server
- [ ] Check Year Joined after year born
- [X] Change Position to drop-down
- [X] Change Age Group to pull-down
- [X] Check Year Born aligns with Age Group
- [ ] Check valid Year Born
- [ ] Change year born / age group message to include year born and age group

### Coach Details

- [X] Mark Fields required
- [X] Create another line when 'Add Team' is clicked
- [X] Remove line when 'Delete' is clicked
- [X] Remove 'Pencil' icon
- [ ] Send data to server

### Referee Details

- [X] Create another line when 'Add Certificate' is clicked
- [X] Remove line when 'Delete' is clicked
- [X] Remove 'Pencil' icon
- [ ] Send data to server